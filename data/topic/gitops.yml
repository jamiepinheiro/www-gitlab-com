description: GitOps is a process of automating IT infrastructure using
  infrastructure as code and software development best practices such as Git,
  code review, and CI/CD pipelines.
canonical_path: /topics/gitops/
file_name: gitops
twitter_image: /images/opengraph/iac-gitops.png
title: What is GitOps?
header_body: >-
  GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.
  


  [Watch our GitOps webcast →](/why/gitops-infrastructure-automation/)

cover_image: /images/topics/gitops-header.png
body: >+
  ## What is GitOps?


  GitOps is an operational framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.


  Modern applications are developed with speed and scale in mind. Organizations with a [mature DevOps culture](https://about.gitlab.com/devops/) can deploy code to production hundreds of times per day. DevOps teams can accomplish this through development best practices such as version control, code review, and CI/CD pipelines that automate testing and deployments.


  While [the software development lifecycle](https://about.gitlab.com/stages-devops-lifecycle/) has been automated, infrastructure has remained a largely manual process that requires specialized teams. With the demands made on today's infrastructure, it's becoming increasingly crucial to implement infrastructure automation. Modern infrastructure needs to be elastic so that it can effectively manage cloud resources that are needed for frequent deployments.


  GitOps is an operational framework that can automate this process of provisioning infrastructure. Similar to how we use application source code, infrastructure teams that practice GitOps use configuration files stored as code (infrastructure as code). GitOps configuration files generate the same infrastructure environment every time it’s deployed, just as application source code generates the same application binaries every time it's built.


  ## GitOps overview


  GitOps is an operational framework that takes DevOps best practices used for application development such as version control, code review, collaboration, compliance, and CI/CD, and applies them to infrastructure automation.


  ### How do teams put GitOps into practice?


  GitOps is not a single product, plugin, or platform. GitOps is a framework that helps teams manage IT infrastructure through processes they already use in application development. To put GitOps into practice, it needs three core components:


  > GitOps = IaC + MRs + CI/CD


  IaC - GitOps uses a [Git repository](https://about.gitlab.com/blog/2020/04/20/ultimate-git-guide/) as the single source of truth for infrastructure definitions. Git is an open source version control system that tracks code management changes, and a Git repository is a `.git` folder in a project that tracks all changes made to files in a project over time. Infrastructure as code (IaC) is the practice of keeping all infrastructure configuration stored as code. The actual desired state may or may not be not stored as code (e.g., number of replicas, pods).


  MRs - GitOps uses [merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/getting_started.html) (MRs) as the change mechanism for all infrastructure updates. The MR is where teams can collaborate via reviews and comments and where formal approvals take place. A merge commits to your master (or trunk) branch and serves as a changelog for auditing and troubleshooting.


  CI/CD - GitOps automates infrastructure updates using a Git workflow with [continuous integration and continuous delivery](https://about.gitlab.com/ci-cd/) (CI/CD). When new code is merged, the CI/CD pipeline enacts the change in the environment. Any configuration drift, such as manual changes or errors, is overwritten by GitOps automation so the environment converges on the desired state defined in Git. GitLab uses CI/CD pipelines to manage and implement GitOps automation, but other forms of automation such as definitions operators can be used as well.

benefits_title: The benefits of GitOps
benefits:
  - title: Enables collaboration on infrastructure changes
    description: Since every change will go through the same change/merge
      request/review/approval process, senior engineers can focus on other areas
      beyond the critical infrastructure management.
    image: /images/icons/gitops-benefits-collaboration.png
  - title: Improved access control
    description: There's no need to give credentials to all infrastructure
      components since changes are automated (only CI/CD needs access).
    image: /images/icons/gitops-benefits-access-control.png
  - title: Faster time to market
    description: Execution via code is faster than manual point and click. Test
      cases are automated and repeatable, so stable environments can be
      delivered rapidly.
    image: /images/icons/gitops-benefits-market.png
  - title: Less risk
    description: All changes to infrastructure are tracked through merge requests,
      and changes can be rolled back to a previous state.
    image: /images/icons/gitops-benefits-risk.png
  - title: Reduced costs
    description: Automation of infrastructure definition and testing eliminates
      manual tasks, improves productivity, and reduces downtime due to built-in
      revert/rollback capability. Automation also allows for infrastructure
      teams to better manage cloud resources which can also improve cloud costs.
    image: /images/icons/gitops-benefits-costs.png
  - title: Less error prone
    description: Infrastructure definition is codified and repeatable, making it
      less prone to human error. With code reviews and collaboration in merge
      requests, errors can be identified and corrected long before they make it
      to production.
    image: /images/icons/gitops-benefits-error.png
cta_banner:
  - title: GitOps challenges
    body: >-
      With any collaborative effort, change can be tricky and GitOps is no
      exception. GitOps is a process change that will require discipline from
      all participants and a commitment to doing things in a new way. It is
      vital for teams to write everything down.


      GitOps allows for greater collaboration, but that is not necessarily something that comes naturally for some individuals or organizations. A GitOps approval process means that developers make changes to the code, create a merge request, an approver merges these changes, and the change is deployed. This sequence introduces a “change by committee” element to infrastructure, which can seem tedious and time-consuming to engineers used to making quick, manual changes.


      It is important for everyone on the team to record what’s going on in merge requests and issues. The temptation to edit something directly in production or change something manually is going to be difficult to suppress, but the less “cowboy engineering” there is, the better GitOps will work.
  - title: What makes GitOps work?
    body: >-
      As with any emerging technology term, GitOps isn't strictly defined the
      same way by everyone across the industry. GitOps principles can be applied
      to all types of infrastructure automation including VMs and containers,
      and can be very effective for teams looking to manage [Kubernetes
      clusters](https://about.gitlab.com/solutions/kubernetes/).


      While many tools and methodologies promise faster deployment and seamless management between code and infrastructure, GitOps differs by focusing on a developer-centric experience. Infrastructure management through GitOps happens in the same version control system as the application development, enabling teams to collaborate more in a central location while benefiting from all the [built-in features of Git](https://devops.com/an-inside-look-at-gitops/).
  - title: Why choose GitLab for GitOps?
    body: >-
      Drive infrastructure automation and collaboration for cloud native,
      multicloud, and legacy environments. GitLab is a DevOps platform with
      built-in agile planning, source code management, and CI/CD delivered as a
      single application. GitLab also has tight integrations with the industry's
      best GitOps tools and cloud platforms.


      Learn how [GitLab can help you adopt GitOps](https://about.gitlab.com/solutions/gitops/).
resources_title: GitOps resources
resources_intro: >-
  Here’s a list of resources on GitOps that we find to be particularly helpful
  in understanding version control and implementation. We would love to get your
  recommendations on books, blogs, videos, podcasts and other resources that
  tell a great version control story or offer valuable insight on the definition
  or implementation of the practice.


  Please share your favorites with us by tweeting us [@gitlab!](https://twitter.com/gitlab)
resources:
  - title: "[Expert Panel Discussion] GitOps: The Future of Infrastructure Automation"
    url: /why/gitops-infrastructure-automation/
    type: Webcast
  - title: "Managing infrastructure through GitOps with GitLab
      and Anthos"
    url: https://about.gitlab.com/webcast/gitops-gitlab-anthos/
    type: Webcast
  - title: "GitLab and HashiCorp - A holistic guide to GitOps and the
      Cloud Operating Model"
    url: /webcast/gitlab-hashicorp-gitops/
    type: Webcast
  - title: "Automating cloud infrastructure with GitLab and Terraform"
    url: /webcast/gitops-gitlab-terraform/
    type: Webcast
  - title: "GitOps with Pulumi and GitLab"
    url: /webcast/gitops-gitlab-pulumi/
    type: Webcast
  - title: "GitOps with AWS and GitLab"
    url: /webcast/gitops-gitlab-aws-helecloud/
    type: Webcast
  - url: /webcast/digital-transformation-northwestern-mutual/
    title: "Accelerating Digital
      Transformation at Northwestern Mutual"
    type: Case studies
  - title: A beginner's guide to GitOps
    url: https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html
    type: Books
  - url: https://youtu.be/JtZfnrwOOAw?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo
    type: Webcast
    title: What is GitOps? Why is it important? How can you get started?
  - url: https://www.youtube.com/watch?list=PLFGfElNsQthbno2laLgxeWLla48TpF8Kz&v=5ykRuaZvY-E&feature=emb_title
    type: Webcast
    title: Using GitLab for GitOps to break down silos and encourage collaboration
suggested_content:
  - url: /blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/
  - url: /blog/2019/11/04/gitlab-for-gitops-prt-1/
  - url: /blog/2019/11/12/gitops-part-2/
  - url: /blog/2019/11/18/gitops-prt-3/
  - url: /blog/2019/10/28/optimize-gitops-workflow/
  - url: /blog/2020/04/17/why-gitops-should-be-workflow-of-choice/
