---
layout: handbook-page-toc
title: Inbound Marketing Handbook
description: Inbound Marketing Handbook
twitter_image: '/images/tweets/handbook-marketing.png'
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---
## On this page 
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# Inbound Marketing Overview
{:.no_toc}
---

## Goals

### Objective

Generate organic traffic and [marketing qualified leads](/handbook/marketing/marketing-operations/#lead-scoring-lead-lifecycle-and-mql-criteria) in support of the company's overall revenue growth goals

### FY21 MQL Targets

| Fiscal Quarter | Large |  Mid-Market |  SMB |
| ------ | ------ |
| Q1FY21 | 5,000 | 3,000 | 11,947 |
| Q2FY21 | 5,500 | 3,300 | 16,157 |
| Q3FY21 | 6,000 | 3,750 | 19,931 |
| Q4FY21 | 6,500 | 4,000 | 21,925 |

*Targets are based on the original FY21 plan. GitLab internal team members can find this information in "FY21 FINAL Marketing: SSOT & Budget"*

### Key Results

Inbound Marketing is responsible for:
* Maintaining and improving GitLab's logged out marketing websites
* Increasing GitLab.com website traffic 20% YoY on an ongoing basis
* Increasing Conversion of Marketing Website visitors to MQLs by 20%

### Deliverables

[Q3FY21 OKRs](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/21)
[Q4FY21 OKRS](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/29)

## Teams
Inbound Marketing is comprised of these Marketing Teams. Visit their handbooks to learn more. 
* [Global Content](/handbook/marketing/inbound-marketing/content/#what-does-the-global-content-team-do)
* [Digital Experience](/handbook/marketing/inbound-marketing/digital-experience/) 
* [Search Marketing](/handbook/marketing/inbound-marketing/search-marketing/)

## Managed Channels

### The Marketing Website
GitLab's Marketing Website (about.gitlab.com) is led by the [Digital Experience Team](/handbook/marketing/inbound-marketing/digital-experience) and anyone can contribute. Please [visit the repo](https://gitlab.com/gitlab-com/www-gitlab-com) to file issues and make merge requests. 

### The Blog 
GitLab's blog is led by the [Global Content Team](/handbook/marketing/inbound-marketing/content). Visit the [Blog Handbook](https://about.gitlab.com/handbook/marketing/blog/) for more information on how you can contribute.


## Communication

### Meeting Cadence

Most of our team meetings are recorded, and can be viewed on GitLab Unfiltered in the [Growth Marketing playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp0T5rN49dxNeJ5uuJ73wMl).

* Monday team leads kickoff to review priorities and blockers
* Friday full team results recap & demo day to share completed work

### The Shared Drive
We have an INTERNAL ONLY Google Shared Drive named **Growth Marketing Shared Drive** where we keep shared documents.

## Process

### When you submit an issue 
You can expect these communications/notifications (either through GitLab or in a comment)

*  You will be provided with specific templates to help you input relevant information
*  Those issues will be vetted at the beginning of each week. The issue will either be: 
   *  Moved into `mktg-status::WIP` or;
   *  A comment will be added for what additional information is needed
*  The issues moved to `mktg-status::WIP` are then put into a sprint and assigned to a week sprint based on priorities, resources, weights and assignment loads
*  The `Assignee` will comment that they have received your Issue and when you can expect more information (ex: "On it. Will respond within the week on timeline")
*  If needed, the Issue Brief will be broken into a Project Epic with relevant issues and those issues reassigned to the relevant DRIs as `Assignee(s)` 
*  A timeline  will be provided in that Project Epic or individual issue and the due date will be adjusted accordingly.
*  When feedback is needed the `Assignee(s)` will comment in the issue(s) asking for your input. They will be as specific as possible with what they are looking for comments on. 
*  Final deliverables will be added in the appropriate form (file, link to repository, web link, etc.) and the Assignee(s) will @ you
*  When final deliverables have been completed the `Assignee(s)` will close the issue. If additional items are needed please open a new issue and relate to each other.  
*  If an issue needs to be moved to the next milestone the `Assignee` will comment in the issue with the reason (ex: problems with testing)

### Requesting Support
Please fill out one of these Issue Templates to request support. Please note, if these are not filled out we won't have the proper information for us to support your request.

#### Website Issue Templates

* [Requesting a new webpage](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-new-website-brief)  
  * Use this template to request new webpages related to logged-out `about.gitlab.com` and related properties such as landing pages for Marketo, Pathfactory, etc.
  * Do NOT use this for logged-in (In-App) gitlab.com or the Handbook
  * For more information on this process click [HERE](LINK)
  * [Image Guidelines](/handbook/marketing/inbound-marketing/digital-experience/image-guidelines/): Please follow these guidelines when providing logos or other images for the website
<br />

* [Requesting an update to an existing webpage](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-update-webpage-brief)  
  * Use this template to update existing webpages related to logged-out `about.gitlab.com` and related properties such as landing pages for Marketo, Pathfactory, etc.
  * Do NOT use this for logged-in (In-App) gitlab.com or the Handbook
  * [Image Guidelines](/handbook/marketing/inbound-marketing/digital-experience/image-guidelines/): Please follow these guidelines when providing logos or other images for the website
<br />

* [Requesting another type of website development project](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-other)
  * Use this template for small quick updates on about.gitlab.com and related properties such as landing pages for Marketo, Pathfactory, etc. For example, adding a logo or singular image to an existing page.
  * Use this template for items that do not fit into other workflows above. Examples of this include research projects, vendor evaluations, documentation requests, tooling or ops related issues, proof-of-concepts, exploratory conversations that aren't yet actionable, etc. 
  * Do NOT use this for logged-in (In-App) gitlab.com or the Handbook
  * Do NOT use this for new webpages or large dev/web design asks
  * [Image Guidelines](/handbook/marketing/inbound-marketing/digital-experience/image-guidelines/): Please follow these guidelines when providing logos or other images for the website
<br />

* [Requesting homepage promotion](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion
   )   
   * Use this template to request to be put on the content calendar for homepage promotion
   * Note: this is a request and must be approved 
   * If this is for a campaign, please put in the request as part of the initial campaign distribution plan 
   * For more information on this process click [HERE](/handbook/marketing/inbound-marketing/content/index.html#homepage-promotion-guidelines)
<br />

* [Report a bug on about.gitlab.com](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report)
   * Please supply all information if possible. The questions might not seem relevant but if we can't reproduce the bug then we can't fix it.
  <br />

* [Request an A/B Test](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/issues/new?issuable_template=ab-test) 
   * Use this template to request an A/B test on the website
   * Note: There are many requests for tests and often the strategy behind pages is rapidly evolving. Your request will be evaluated and logged in the Page Testing Spreadsheet. A/B testing spreadsheets are located in the Shared Growth Marketing Drive.
   * Process for A/B Tests (LINK COMING SOON)
<br />

#### Brand and Design Issue Templates 
The brand and design team is now the Brand Activation team under Corporate Marketing. Please see their [Handbook page](/handbook/marketing/corporate-marketing/brand-activation/brand-design/#requesting-support) for requesting support. 
#### Inbound Marketing Issue Templates
  * [Request a redirect](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/issues/new?issuable_template=set-up-a-new-redirect)
  * [Request a Hotjar heatmap](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/issues/new?issuable_template=hotjar-heatmap-request)
  * [Request keyword research](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/issues/new?issuable_template=keyword-research-request)
  * [Request on page optimization research](https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/issues/new?issuable_template=on-page-optimization)
  * Request A/B Test: see above in [Website Request Templates](https://about.gitlab.com/handbook/marketing/inbound-marketing/#website-issue-templates) 

#### Video Issue Templates
  * [Requesting a new video](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/new?issuable_template=new-video-request)
    * Use this template only for requesting new videos that haven't been produced yet
    * If you are looking for edits to existing content that has been filmed, use the [video editing request](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/new?issuable_template=video-editing-request)
    * Process documetation link placeholder 
  * [Requesting a video edit](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/new?issuable_template=video-editing-request)   
    * Use this template for editing existing content that has been filmed 
    * DO NOT use this template for new videos
    * Process documetation link placeholder 
  *  [Requesting a video upload](https://gitlab.com/gitlab-com/marketing/inbound-marketing/global-content/digital-production/-/issues/new?issuable_template=upload-request)
     * Process documetation link placeholder 


# How we use GitLab 

## Boards
Overarching Inbound Marketing Boards

#### [Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/1761578?&label_name[]=mktg-inbound&label_name[]=mktg-status%3A%3Atriage) 

*  **GOAL:** Weed out issues that superfluous or don’t contain extra information 
*  **ACTIONS:** Move to either
   *  Move to either `mktg-status:WIP` which puts into the Sprint cycle or;
   *  [mktg-status: blocked] and comment why it is being blocked

#### [Sprint Board](https://gitlab.com/groups/gitlab-com/-/boards/1761580?label_name[]=mktg-inbound&label_name[]=mktg-status%3A%3Awip)
*  **GOAL:** Assign issues to milestones to be worked on then based on due dates, weights, priorities, resources
*  **ACTIONS:** 
   *  Move to a specific week taking into consideration the above 
   *  Backlog if:
      *  It doesn’t fit into current goals or priorities
      *  We don't have the time or resources currently
      *  It is just a cool idea (some of these may be labeled for future review for upcoming strategies & tactics) 
   *  Comment why it is backlogged
   *  Never backlog PR issues without prior discussion

   > HANDOFF: This is where the handoff to Inbound Marketing Teams takes place to run their processes

#### [Inbound Marketing Leads Overview](https://gitlab.com/groups/gitlab-com/-/boards/1787553?milestone_title=%23started&&label_name[]=mktg-inbound&label_name[]=mktg-status%3A%3Awip)
* **GOAL:** An overview of the work on each of the Inbound Marketing Teams during the current Milestone Sprint
* **ACTIONS:**
  * Review and help prioritze
  
## Labels
Meanings and usage

*  `mktg-inbound`
   *  Denotes it could be part of the Inbound Marketing Teams scope. 
   *  Used to help pull in boards
   *  Is not a label to denote the status of an issue
*  `mktg-status::triage`
   *  Universal marketing label
   *  Is used to denote the status of an issue as being reviewed to go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::WIP`
   *  Universal marketing label
   *  Is used to denote the status of an issue as having been reviewed for all details needed and will go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::blocked`
   *  Universal marketing label
   *  Is used to denote the status of an issue as not having enough information to proceed
   *  Does not denote the timeline it will be worked on
   *  This should ONLY be used in the Triage process or if an issue becomes so blocked it needs to be re-scheduled. 
      * If an issue is blocked it goes back into the Triage process and is scheduled from there
*  `mktg-status::review`
   *  Universal marketing label
   *  Is used to denote the status of an issue as in the peer-review process.
   *  Does not denote the timeline it will be worked on
*  [Group Labels] `design`, `web-analytics`, `mktg-website`, `mktg-content`, `mktg-inbound`
   *  Denotes it could be part of a Inbound Marketing Group scope
   *  Is not a label to denote the status of an issue
   * [Individual Team Labels] `content marketing`, `editorial`, `digital-production`, `Brand and Digital Design`
      * Denotes it could be part of Inbound Marketing Group Team's scope
      * Is not a label to denote the status of an issue
*  `IM-Support`
   *  Denotes if this is support provided to another team outside of Inbound Marketing
   *  Will be used to help quickly view how many issues Inbound Marketing supported each year to provide as references for budget and resources requests.  
*  `IM-Review-Future`
   *  Will be reviewed by InboundMarketing for potential future strategy
*  `IB-WEBAGENCY`
   * For the Digital Experience team to support at their 20% time allotment for internal agency work.
## Milestones
Meanings and usage

* `Fri: MONTH DAY`
   * Denotes which week an issue will be worked on 
   * Worked on = will be broken down into smaller Issues or Issue will be completed and closed
* `Inbound Marketing Backlog` 
   *  Denotes that this not fit into current goals,  time, or resources - can be reviewed later for informing future goals
   *  Does NOT mean the issue request is cancelled
*  `Case Study`
   *  ONLY for Case Studies 
   *  Created because they span multiple months and teams and timelines cannot be accurately assessed until towards the end
   *  New issues with sprint milestones will be made for publishing the case study
*  `INFORMATION GATHERING`
   *  Denotes issues used to gather information, but do not have set deliverables 

## Epics
This is how we use Epics to organize our projects. We aim to include the highest level information at the top level and specific information in lower-level epics. This is to 
keep a SSoT and avoid duplicative placement of information that would need to be updated in multiple places

* **Top Level Epic**
  * Quarterly OKRs
  * Links to the yearly strategy that contains themes
* **Second Tier Epic**
  * Stores the team or channel strategy/improvements broken into phases
  * Includes
    * Current state
    * Stakeholders
    * Metrics - Baseline and Target
    * Desired state - Key audience + Intent and Key company goals
    * Improvement Roadmap - broken out by quarter if it will cross multiple
    * Phases - per quarter and includes: 
      *   The Problem
      *   The Solution 
      *  Why this Effort
* **Third Tier Epics**
  * Epic per phase - one epic per phase
  * Contains:
   * Scope of project
   * Roles & Responsibilities Chart if different than the parent epic
   * Relevant Documents
   * High-level epic production only - All production for individual issues should be placed in those issues to avoid redundancy 
* **Fourth Tier Epics**
  * MVCs for project - one epic per MVC  
  * Contains: 
    * Scope of MVC
    * Relevant Documents
    * High-level epic production only - All production for individual issues should be placed in those issues to avoid redundanc
* **Fifth Tier Epics**
  * Production issues 
  



## Issues 

### Issue Types
This is how we work with Issues and their scope
#### External
(outside of Inbound Marketing)
**REQUEST ONLY ISSUE**
*  This is for complicated projects (example: campaign). 
*  This type of Issue is submitted by a team OUTSIDE of Inbound Marketing. 
*  This serves as a brief to us. 
*  For each type of request we have a corresponding  **Request [Type] Issue Template** that should be filled out to begin the process. 
     * [Requesting Support](/handbook/marketing/inbound-marketing/#requesting-support)
* Actions taken on issue: 
  * Reviewed for all information and label changed to `mktg-status::WIP` if all there or `mktg-status::blocked` if not all info is there
  * Assigned to a team member to execute on
  * Assigned to a Sprint week to be worked on (worked on= broken down into Production Issues)
  * Issue is added to a child Epic and execution issues are made for creating
  * Assignee will comment and close the Request Issue and deliver the work in the Child Epic

**REQUEST AND PRODUCTION ISSUE**
* This is for a simple ask (example: single asset)
* This type of Issue is submitted by a team outside of Inbound Marketing and will contain relevant information for the project AND;
* Execution Items/Production Checklists for the Inbound Marketing Teams.
* For each type of request we have a corresponding **Request [Type] Issue Template** that should be filled out to begin the process. 
* Actions taken on issue: 
  * Reviewed for all information and label changed to `mktg-status::WIP` if all there or `mktg-status::blocked` if not all info is there
  * Assigned to a team member to execute on
  * Assigned to a Sprint week to be worked on (worked on = deliverables created)
  * Assignee will follow the production checklist and deliver work within the original Issue

#### Internal
(inside of Inbound Marketing)

**PRODUCTION ISSUE**
 * This is an Issue we use internally to create and execute on the request/brief. 
 * These are only used by people inside of Inbound Marketing 
 * For each type of request we have a corresponding  **Production [Type] Issue Template** that should be filled out to begin the process. 
 * The teams will deliver the individual pieces to these Issues, but not the completed ask. That should be delivered into the Child Epic created


## Issue Templates

#### External: 
(outside of Inbound Marketing): 

**Issue Author**
* Pick template based on Handbook directions 
  * Template tags will be automatically added:
    * `mktg-inbound`
    * `mktg-status::triage`
    * The relevant Group Label: `design`, `content marketing`, `mktg-website`, or `mktg-analytics`
    * `IM-Support` 
  * Template contains specific information to help fill out including directions for picking a due date
  * Template has auto-assignees
  * Links to Handbook page for additional information

#### Internal: 
(inside of Inbound Marketing): 

**Assignees of Issues:**
*  Pick template for breakout of brief
  *  Template tags: 
     *  Keep existing above EXCEPT if we start the project then we remove `MG-Support`
     *  Add your team's process tags
* If the ask (External brief Issue) requires one ask:
  * Re-assign as need, make sure it is in a sprint week and follow documented processes
* If the ask (External brief Issue) requires more than one ask:
   * Create an Epic to hold project
   * Copy and paste markdown into Epic Description and use header: “External Brief”
   * Associate Issue with Epic 
   * Comment Close Issue that the project epic has been created
   * Build out necessary Issues in project, Associate with Epic, assign Issues and update Epic Description as needed with additional brief information 


# Process

## Weekly Process

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xeFP0-Kdq-M" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**Beginning of week:** 

*  Geekbot Check-In
* Triage board is vetted and items moved (see above for details on actions)
*  Inbound Team Leadership meeting: 
   *  Discuss priorities for the week, Danielle relays exec and PR info, we decide if any project, company or larger Mkrg priorities are bubbling up that would make us reconsider priorities 
   *  Groom issues into weeks using the Sprint Board 

> HANDOFF: This is where the handoff to Inbound Marketing Teams takes place to run their processes

*  Inbound Marketing Leads meets with their teams (anyway they want: meeting, slack, async, geek bot) and relays that information
*  Teams: Assign (if not auto-assigned) Issues, Projects, Tasks and follow their process 


**End of week:**
*  Confirm if everything has been closed and move to next milestone sprint if not completed
*  Comment in issue why it was moved to the next milestone (ex: person x out sick, complications with testing, etc.)

## Quarterly Process 
* Epics for the quarterly OKRs are created and links are added to the yearly strategy in the handbook
* Child epics are created per the [epic structure](/handbook/marketing/inbound-marketing/#epics) listed above

## Yearly Process 
* At the beginning of Q4, we will create a Roadmap Epic for all items that will be candidates for next year's strategy.
* Strategy for the year is documented in the handbook

# Video Playlists

In an effort to make async communication and collaboration as effective as possible, below are relevant playlists created by the Inbound Marketing teams. We welcome suggestions for additional playlists and videos that would help your team. Please request in our [slack channel](https://gitlab.com/gitlab-com/marketing/inbound-marketing) 

* [Weekly Inbound Marketing Recap and Demo](https://www.youtube.com/playlist?list=PL05JrBw4t0KppgWkSa3YgDgc_qUTKsBCs)
* [SEO Update](https://www.youtube.com/playlist?list=PL05JrBw4t0KpoehEW7O8eA5Iy2FP_u6sW)
* Marketing Metrics
* [Digital Experience Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba)
* [Global Content Updates](https://www.youtube.com/playlist?list=PL05JrBw4t0KrzoCLxdlF5eJjh0JDgn4Qs)

# Vendor Management 

### Vendor recruitment

If you know of a brand or digital vendor, whether “an agency of one” (aka freelancer) or agency, please share the contact with your manager for further consideration.
    
### Vendor on-boarding

1. Manager creates [Access Request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) for relevant GitLab projects and Slack channels
2. Manager creates Intro to GitLab issue <link>
3. Intro call with Manager, Vendor, and Vendor Buddy to set expectations, outline workflow, share relevant handbook links, and introduce first project(s).
4. Vendor completes Intro to GitLab issue, including standard and design and or dev on-boarding tasks (depending on vendor type).
5. Help any developer vendors if they have trouble setting up their code environment and please familiarize them with our codebase.
6. Questions about on-boarding, please see handbook, your manager, and or thge [#marketing_brand_contractor_onboarding](https://gitlab.slack.com/archives/C0114QFK8M6) channel.

### Vendor project management

#### Team

- **Assigning Issues to Vendors**
    - Assign the vendor and team member who will be the vendor’s buddy.
    - Ensure following labels are on the issue:
        - `vendor`
        -  `mktg-inbound`, `mktg-status`
        - `design` and or `mktg-website`
    - Assign the issue to a milestone, as appropriate.
    - Confirm the issue appears on the [vendor board](https://gitlab.com/groups/gitlab-com/-/boards/1511334?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-inbound&label_name[]=outsource). 
    - Capture opportunities to improve vendor management in the [vendor management epic](https://gitlab.com/gitlab-com/marketing/inbound-marketing/brand-and-digital/brand-and-digital/-/issues/248).
        

#### Vendor buddy

- **Managing Vendor Work**
    - Ensure you and vendor are assigned to the issue.
    - Check-in regularly to ensure progress and clear blockers.
    - Ensure projects to which you’re assigned are maintained on the [vendor board](https://gitlab.com/groups/gitlab-com/-/boards/1511334?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-inbound&label_name[]=outsource), just as you maintain your own projects.
    - Communicate with vendor in issues and or the vendor-contract slack channel, [#brand-contractors](https://gitlab.slack.com/archives/C011PDBLWLC), per GitLab’s async communication best practices. Also be aware GitLab’s async model may be new to contractors. Be open to synchronous meetings as vendors acclimate to async work.
    - Meet standard GitLab and Brand and Digital work practices, including open and inclusive design and **weekly status updates**.
    - Review vendor deliverables for quality assurance.
- **Addressing Concerns**
    - Should issues arise, provide vendors feedback in private. Be specific on the concern, how it can be resolved, and set a date for when it will be resolved.
    - Share critical concerns or patterns of issues with your manager.
    - Immediately report urgent concerns to your manager, such as significant project delays or poor quality of work.

#### Managers

- **Vendor Boards** - Ensure [vendor board](https://gitlab.com/groups/gitlab-com/-/boards/1511334?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-inbound&label_name[]=outsource) is maintained.
- **Vendor Issues** - Ensure the `vendor` label is being applied to issues we *might* outsource.
- **Vendor ROI -** Ensure we are planning sprints to maximize vendor output while minimizing vendor overhead.
- **Monthly Vendor Work Report** - Provide end of month reports on what each vendor completed, and any issues encountered that require stakeholder attention.


### Vendor invoicing, payment and reporting


#### Vendor buddy

- Ensure you know enough about the [invoicing process](/handbook/finance/accounting/#invoice-accounting) to assist when directing invoicing questions from the vendor to the appropriate party.
    - Please don’t drop the ball, we want vendors to be happy and get paid.
    - Please follow any questions from start to finish to keep things moving along.
        - Ex: “I’m having trouble with Tipalti, who can help me?” (Lori Lamb)
        - Ex: “Do I need a purchase order number for my invoice?”

#### Managers

- **Vendor contracts** - Negotiate and execute [vendor contracts](/handbook/finance/procure-to-pay/), with Director’s approval. Goals are to maximize value delivered within our allocated vendor budget.
- **Invoicing** - Ensure vendors:
    - [Set themselves up properly in Tipalti](/handbook/finance/accounting/#3-vendor-master-management)
    - Adhere to GitLab’s invoicing standards
    - Invoice all remaining hours by the end of month
- **Monthly vendor work report**
    - Provide director with end of month report of vendor projects, including projects in progress and those completed within the past month.
    

#### Director

- **Approve invoices** 
    - Approve invoices in Tipalti throughout the month and at end of month.  
    - Validate vendor invoices and deliverables with  Brand and Digital managers.
    - Ensure projects are billed to the appropriate budgets.
- **Approve contracts**
    - Engage vendors in alignment with brand and business objectives.
    - Manage overall budget spend.
- **Monthly vendor work report** 
    - Add spend to Monthly Vendor Work Report and distribute to stakeholders for awareness. Stakeholders include Inbound leadership, Finance, and Marketing team leads working with vendors.

---
Return to the main [Marketing Handbook](/handbook/marketing/).


