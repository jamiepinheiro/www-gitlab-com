---
layout: handbook-page-toc
title: "Account Based Strategy"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Strategy
The account based strategy team is responsible for: 
- Developing (in partnership with sales and marketing) and managing our [ideal customer profile (ICP)](/handbook/marketing/revenue-marketing/account-based-strategy/#ideal-customer-profile)
- [Account based marketing (ABM)](/handbook/marketing/revenue-marketing/account-based-strategy/#account-based-marketing-abm) campaigns and tactics
- [Demandbase](/handbook/marketing/revenue-marketing/account-based-strategy/#demandbase) - account based engagement, intent and campaign orchestration
- Maintenance of GitLab's [focus account lists](#focus-account-lists-gl4300--mm4000), titled the GL4300 and MM4000.

## Account Based Marketing (ABM)
- [ABM handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/)
Account-based marketing is a focused B2B marketing approach in which marketing and sales teams work together to target key or "best-fit" accounts. These accounts are marketed to directly, as a market of one (compared to the typical one-to-many approach). 

## Demandbase
- [Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/)
Demandbase is a complete end-to-end solution for Account-Based Marketing. We primarily use Demandbase as a targeting and personalization platform to target online ads to companies that fit our ICP and tiered account criteria. Demandbase also has a wealth of intent data that is available to us through its integration with Salesforce.

## Focus account lists: GL4300 & MM4000
- [Handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/GL4300-and-MM4000./)
Focus list of accounts that are [first order logos](/handbook/sales/#first-order-customers) (first paid customer within an organziation) for GitLab and fit our ideal customer profile for both Large (GL4300) and MidMarket (MM4000). The lists are developed using the current data sources we have at our disposal and are refreshed quarterly at the beginning of each quarter.

## Ideal Customer Profile
An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our focus list of highest value accounts. 

**A few things to note about GitLab's ICP:** 
- it is that it is fairly broad, mainly because GitLab can ultimately sell to a vast number of companies versus say, a banking solution that would have a much smaller TAM (total addressable market)  
- our ICP is hyper focused on first order logos
- Because of our large TAM we do not focus on ALL the accounts that fit our ICP at a given time, but rather, focus on a subset based on different variables including propensity to buy and additional intent data.

### Large

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of developers (currently also using company size as proxy) | 500+ |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion |
| | Cloud provider | AWS or GCP |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | Digital transformation | identified C-suite initiative | 
| | High intent account | Account is trending as high intent based on our data in Demandbase |

### Mid Market

|  | **Attribute** | **Description** |
| ------ | ------ | ------ |
| **Core criteria (must haves)** | Number of employees | >500 |
| | Tech stack (regional) | Includes GitHub, Perforce, Jenkins, BitBucket or Subversion.  Also include a LACK of tech stack in smaller companies |
| | Prospect | First order logo (not a current PAID customer for GitLab anywhere within the organization)  |
| **Additional criteria (attributes to further define)** | New hire | CIO | 
| | High intent account | Account is trending as high intent based on our data in Demandbase |

